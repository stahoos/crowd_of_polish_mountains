-include .env
export

include makefiles/help/Makefile

BIN_CONSOLE=bin/console -v
CONTENER_NAME=inst.dmp-backoffice.api-dev

ifeq ("${PWD}", "/var/www/app")
	PRE_BIN_CONSOLE_EXEC=
	PRE_COMPOSER_EXEC=
else
	PRE_BIN_CONSOLE_EXEC=docker exec ${CONTENER_NAME}
	PRE_COMPOSER_EXEC=docker run --rm --interactive --tty --volume ${PWD}:/app --volume ${COMPOSER_HOME}:/tmp
endif




# Symfony setup commands
.PHONY: composer composer-require composer-remove composer-require-dev composer-upgrade create-vendor-folder security-check cache-warmup

## Setup:	Install all vendors required by Symfony
composer: create-vendor-folder
	${PRE_COMPOSER_EXEC} composer install --prefer-dist --no-scripts --no-progress --no-suggest

# Extract extra arguments
# accept parameters
ifeq ($(firstword $(MAKECMDGOALS)),$(filter $(firstword $(MAKECMDGOALS)),composer-require))
  ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(ARGS):;@:)
endif

## Setup:	composer require (by composer container)
composer-require:
	${PRE_COMPOSER_EXEC} composer require --prefer-dist --no-scripts --no-progress --no-suggest ${ARGS}

# Extract extra arguments
# accept parameters
ifeq ($(firstword $(MAKECMDGOALS)),$(filter $(firstword $(MAKECMDGOALS)),composer-remove))
  ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(ARGS):;@:)
endif

## Setup:	composer remove (by composer container)
composer-remove:
	${PRE_COMPOSER_EXEC} composer remove --no-scripts --no-progress ${ARGS}

# Extract extra arguments
# accept parameters
ifeq ($(firstword $(MAKECMDGOALS)),$(filter $(firstword $(MAKECMDGOALS)),composer-require-dev))
  ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(ARGS):;@:)
endif

## Setup:	composer require-dev (by composer container)
composer-require-dev:
	${PRE_COMPOSER_EXEC} composer require-dev --prefer-dist --no-scripts --no-progress --no-suggest ${ARGS}

## Setup:	Upgrade all vendors required by Symfony
composer-upgrade: create-vendor-folder
	${PRE_COMPOSER_EXEC} composer upgrade --prefer-dist --no-scripts --no-progress --no-suggest

create-vendor-folder:
	mkdir -p ${PWD}/vendor

## Setup:	security check packages
security-check:
	${PRE_COMPOSER_EXEC} composer run-script security-check

## Setup:	Cache warmup
cache-warmup:
	${PRE_BIN_CONSOLE_EXEC} ${BIN_CONSOLE} cache:warmup




.PHONY: setup-db drop-db migrate-db

## Setup DB:	all dbs and sample users for developing/testing
setup-db: migrate-backend migrate-taxonomies import-sample-5k-taxonomies import-sample-taxonomies import-sample-segments refresh-materialized-view create-sample-users

## Setup DB:	drop databases
drop-db:
	${PRE_BIN_CONSOLE_EXEC} ${BIN_CONSOLE} doctrine:database:drop --connection default --if-exists --force
	${PRE_BIN_CONSOLE_EXEC} rm -f var/cache/test/database.db
	# todo if sqlite

## Setup DB:	Run db migations
migrate-db:
	${PRE_BIN_CONSOLE_EXEC} ${BIN_CONSOLE} doctrine:database:create --connection default --if-not-exists
	${PRE_BIN_CONSOLE_EXEC} ${BIN_CONSOLE} doctrine:migrations:migrate --db default


# Developer tools
.PHONY: php-cs-sniff php-cs-fixer phpunit composer-and-build-and-run build-and-run run build-run-containers run-containers stop-and-destroy
## Develop:	PHP Coding Standard checker (only for developers)
php-cs-sniff:
	vendor/bin/phpcs --standard=phpcs.xml --extensions=php --colors src/ tests/

## Develop:	PHP Coding Standard fixer (only for developers)
php-cs-fixer:
	vendor/bin/php-cs-fixer fix --diff --show-progress=estimating --rules=@Symfony

# Extract extra arguments
# accept parameters
ifeq ($(firstword $(MAKECMDGOALS)),$(filter $(firstword $(MAKECMDGOALS)),phpunit))
  ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(ARGS):;@:)
endif

## Develop:	Run PhpUnit (tests) in container (only for developers)
phpunit:
	${PRE_BIN_CONSOLE_EXEC} bin/phpunit ${ARGS}

server-start:
	bin/console server:start

## Develop:	Composer & Build & Run whole stack and setup dbs
composer-and-build-and-run: composer build-run-containers wait setup-db setup-db-test

## Develop:	Build & Run whole stack and setup dbs
build-and-run: build-run-containers wait setup-db setup-db-test

## Develop:	Run whole stack and setup dbs
run: run-containers wait setup-db setup-db-test

build-run-containers:
	docker-compose up -d --build

run-containers:
	docker-compose up -d

wait:
	sleep 30

## Develop:	Destroy containers
stop-and-destroy:
	docker-compose down
