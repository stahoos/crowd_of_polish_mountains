<?php

namespace App\Util\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;
use Ulid\Ulid;

/**
 * ULID generator for the Doctrine ORM.
 */
class UlidGenerator extends AbstractIdGenerator
{
    /**
     * @param EntityManager $em
     * @param object|null   $entity
     *
     * @return mixed|Ulid
     */
    public function generate(EntityManager $em, $entity)
    {
        return Ulid::generate();
    }
}
