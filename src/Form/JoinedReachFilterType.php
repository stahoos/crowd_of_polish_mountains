<?php

namespace App\Form;

use App\Dictionary\AuditLogActionTypes;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Form\Filter\Type\FilterType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ReachFilterType
 * @package App\Form
 */
class JoinedReachFilterType extends FilterType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'choices' => [
                    'yes' => 'yes',
                    'no' => 'no'
                ]
            ]
        );
    }

    /**
     * @return string|null
     */
    public function getParent()
    {
        return ChoiceType::class;
    }

    /**
     * @inheritDoc
     */
    public function filter(QueryBuilder $queryBuilder, FormInterface $form, array $metadata)
    {
        if ($form->getData() === 'yes') {
            $queryBuilder->andWhere('finish.reach_date IS NOT NULL');
        } else {
            $queryBuilder->andWhere('finish.reach_date IS NULL');
        }
    }
}
