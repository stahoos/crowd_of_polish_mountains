<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ulid\Ulid;
use Location\Coordinate;
use Location\Distance\Vincenty;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlaceRepository")
 */
class Place
{
    /**
     * @var Ulid
     *
     * @ORM\Column(type="string", length=26)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator("App\Util\Doctrine\UlidGenerator")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mountain_range;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $altitude;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $reach_date;

    private $reach;

    /**
     * @ORM\Column(type="boolean")
     */
    private $peak;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=6)
     */
    private $lat;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=6)
     */
    private $long;

    private $home;

    private $distance;

    /**
     * ORM\OneToMany(targetEntity="App\Entity\Distance", mappedBy="start")
     */
//    private $distances;

    public function __construct()
    {
        $this->home = new Coordinate(51.0810213,17.0797553);
        $this->distances = new ArrayCollection(); // wroclaw
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMountainRange(): ?string
    {
        return $this->mountain_range;
    }

    public function setMountainRange(?string $mountain_range): self
    {
        $this->mountain_range = $mountain_range;

        return $this;
    }

    public function getAltitude(): ?int
    {
        return $this->altitude;
    }

    public function setAltitude(?int $altitude): self
    {
        $this->altitude = $altitude;

        return $this;
    }

    public function getReachDate(): ?\DateTimeInterface
    {
        return $this->reach_date;
    }

    public function setReachDate(?\DateTimeInterface $reach_date): self
    {
        $this->reach_date = $reach_date;

        return $this;
    }

    public function getReach()
    {
        return !empty($this->reach_date);
    }

    public function getPeak(): ?bool
    {
        return $this->peak;
    }

    public function setPeak(bool $peak): self
    {
        $this->peak = $peak;

        return $this;
    }

    public function getLat()
    {
        return $this->lat;
    }

    public function setLat($lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLong()
    {
        return $this->long;
    }

    /**
     * @param mixed $long
     * @return Place
     */
    public function setLong($long)
    {
        $this->long = $long;
        return $this;
    }

    public function getDistance()
    {
        $home = new Coordinate(51.0810213,17.0797553);
        $peak = new Coordinate($this->lat, $this->long);

        $calculator = new Vincenty();

        return sprintf("%s km", floor($calculator->getDistance($home, $peak)/1000));
    }

    public function __toString()
    {
        $array[] = $this->name;
        $array[] = $this->mountain_range;
        $array[] = $this->lat;
        $array[] = $this->long;

        return implode(" | ", array_filter($array));
    }
//    /**
//     * @return Collection|Distance[]
//     */
//    public function getDistances(): Collection
//    {
//        return $this->distances;
//    }
//
//    public function addDistance(Distance $distance): self
//    {
//        if (!$this->distances->contains($distance)) {
//            $this->distances[] = $distance;
//            $distance->setStart($this);
//        }
//
//        return $this;
//    }
//
//    public function removeDistance(Distance $distance): self
//    {
//        if ($this->distances->contains($distance)) {
//            $this->distances->removeElement($distance);
//            // set the owning side to null (unless already changed)
//            if ($distance->getStart() === $this) {
//                $distance->setStart(null);
//            }
//        }
//
//        return $this;
//    }
}
