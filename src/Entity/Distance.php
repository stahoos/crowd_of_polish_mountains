<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ulid\Ulid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DistanceRepository")
 */
class Distance
{
    /**
     * @var Ulid
     *
     * @ORM\Column(type="string", length=26)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator("App\Util\Doctrine\UlidGenerator")
     */
    private $id;

    /**
     * ORM\ManyToOne(targetEntity="App\Entity\Place", inversedBy="distances")
     * @ORM\ManyToOne(targetEntity="App\Entity\Place")
     */
    private $start;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Place")
     */
    private $finish;

    /**
     * @ORM\Column(type="integer")
     */
    private $distance;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getStart(): ?Place
    {
        return $this->start;
    }

    public function setStart(?Place $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getFinish(): ?Place
    {
        return $this->finish;
    }

    public function setFinish(?Place $finish): self
    {
        $this->finish = $finish;

        return $this;
    }

    public function getDistance()
    {
        return $this->distance;
    }

    public function setDistance($distance): self
    {
        $this->distance = $distance;

        return $this;
    }
}
