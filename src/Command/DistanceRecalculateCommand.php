<?php

namespace App\Command;

use App\Entity\Distance;
use App\Entity\Place;
use Doctrine\ORM\EntityManagerInterface;
use Location\Coordinate;
use Location\Distance\Vincenty;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DistanceRecalculateCommand extends Command
{
    protected static $defaultName = 'distance:recalculate';

    protected function configure()
    {
        $this
            ->setDescription('Recalculate distances between points')
//            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
//            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * DistanceRecalculateCommand constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $placeRepository = $this->em->getRepository(Place::class);
        $distanceRepository = $this->em->getRepository(Distance::class);

        $distanceRepository->deleteAll();

        $places = $placeRepository->findAll();

        /** @var Place $start */
        foreach ($places as $start) {
            /** @var Finish $finish */
            foreach ($places as $finish) {
                if ($start != $finish) {
                    $startCoord = new Coordinate($start->getLat(), $start->getLong());
                    $finishCoord = new Coordinate($finish->getLat(), $finish->getLong());

                    $calculator = new Vincenty();

                    $distance = new Distance();
                    $distance
                        ->setStart($start)
                        ->setFinish($finish)
                        ->setDistance(floor($calculator->getDistance($startCoord, $finishCoord)/1000))
                    ;

                    $this->em->persist($distance);
                }
            }
        }

        $this->em->flush();


//        $arg1 = $input->getArgument('arg1');

//        if ($arg1) {
//            $io->note(sprintf('You passed an argument: %s', $arg1));
//        }

//        if ($input->getOption('option1')) {
//             ...
//        }

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');
    }
}
